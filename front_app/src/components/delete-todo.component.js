import React, { Component } from 'react';
import Moment from 'moment';
import axios from 'axios';

export default class DeleteTodo extends Component {

    constructor(props) {
        super(props);

        this.onChangedublin_titulo = this.onChangedublin_titulo.bind(this);
        this.onChangedublin_claves = this.onChangedublin_claves.bind(this);
        this.onChangedublin_descripcion = this.onChangedublin_descripcion.bind(this);
        this.onChangedublin_fuente = this.onChangedublin_fuente.bind(this);
        this.onChangedublin_tipo_recurso = this.onChangedublin_tipo_recurso.bind(this);
        this.onChangedublin_cobertura_fechaini = this.onChangedublin_cobertura_fechaini.bind(this);
        this.onChangedublin_cobertura_fechafin = this.onChangedublin_cobertura_fechafin.bind(this);
        this.onChangedublin_cobertura_ubicacion = this.onChangedublin_cobertura_ubicacion.bind(this);

        this.onSubmit = this.onSubmit.bind(this);
    
        this.state = {
            dublin_titulo: '',
            dublin_claves: '',
            dublin_descripcion: '',
            dublin_fuente: '',
            dublin_tipo_recurso: '',
            dublin_cobertura_fechaini: '',
            dublin_cobertura_fechafin: '',
            dublin_cobertura_ubicacion: ''
        }
    }

    componentDidMount() {
        //axios.get('http://localhost:4000/todos/'+this.props.match.params.id)
        axios.get('https://crud01rsanta.herokuapp.com/todos/'+this.props.match.params.id)
            .then(response => {
                //var claves = JSON.stringify(response.data.dublin_claves);
                var claves = response.data.dublin_claves.join(';');
                console.log(claves);
                
                var dt  = response.data.dublin_cobertura.cobertura_fechaini.split(/\-|\T/); 
                var  fechaini0 = dt[0]+ '-' + dt[1] + '-' +dt[2];
                
                var dt2  = response.data.dublin_cobertura.cobertura_fechafin.split(/\-|\T/); 
                var  fechaini2 = dt2[0]+ '-' + dt2[1] + '-' +dt2[2];

                this.setState({
                    dublin_titulo: response.data.dublin_titulo,
                    dublin_claves: claves,
                    dublin_descripcion: response.data.dublin_descripcion,
                    dublin_fuente: response.data.dublin_fuente,
                    dublin_tipo_recurso: response.data.dublin_tipo_recurso,
                    dublin_cobertura_fechaini: fechaini0,
                    dublin_cobertura_fechafin: fechaini2,
                    dublin_cobertura_ubicacion: response.data.dublin_cobertura.cobertura_ubicacion
                    });   
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    onChangedublin_titulo(e) {
        this.setState({
            dublin_titulo: e.target.value
        });
    }
    
    onChangedublin_claves(e) {              
        this.setState({
            dublin_claves: e.target.value
        });
    }
    onChangedublin_descripcion(e) {
        this.setState({
            dublin_descripcion: e.target.value
        });
    }
    onChangedublin_fuente(e) {
        this.setState({
            dublin_fuente: e.target.value
        });
    }
    onChangedublin_tipo_recurso(e) {
        this.setState({
            dublin_tipo_recurso: e.target.value
        });
    }
    onChangedublin_cobertura_fechaini(e) {
        this.setState({
            dublin_cobertura_fechaini: e.target.value
        });
    }

    onChangedublin_cobertura_fechafin(e) {
        this.setState({
            dublin_cobertura_fechafin: e.target.value
        });
    }

    onChangedublin_cobertura_ubicacion(e) {
        this.setState({
            dublin_cobertura_ubicacion: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();
        var claves = this.state.dublin_claves.split(";");
        const newDublin = {
            dublin_titulo: this.state.dublin_titulo,
            dublin_claves: claves,
            dublin_descripcion: this.state.dublin_descripcion,
            dublin_fuente: this.state.dublin_fuente,
            dublin_tipo_recurso: this.state.dublin_tipo_recurso,
            dublin_cobertura: {
                    cobertura_fechaini: this.state.dublin_cobertura_fechaini,
                    cobertura_fechafin: this.state.dublin_cobertura_fechafin,
                    cobertura_ubicacion: this.state.dublin_cobertura_ubicacion
                    }
            }; 
        console.log(newDublin);
        //axios.post('http://localhost:4000/todos/update/'+this.props.match.params.id, newDublin)
        axios.post('https://crud01rsanta.herokuapp.com/todos/delete/'+this.props.match.params.id, newDublin)
        
            .then(res => console.log(res.data));
        
        this.props.history.push('/');
    }

    

    render() {
        return (
            <div>
                <h3 align="center">Borrar documento</h3>
                <form onSubmit={this.onSubmit}>
                <div className="form-group"> 
                    <label>Titulo: </label>
                    <h3>{this.state.dublin_titulo}</h3>
                    {/* <input  type="text"
                            className="form-control"
                            value={this.state.dublin_titulo}
                            onChange={this.onChangedublin_titulo}
                            /> */}
                </div>
                <div className="form-group">
                    <label>Claves: [separada por ;] </label>
                    <h3>{this.state.dublin_claves}</h3>
                    {/* <input 
                            type="text" 
                            className="form-control"
                            value={this.state.dublin_claves}
                            onChange={this.onChangedublin_claves}
                            /> */}
                </div>
                <div className="form-group">
                    <label>Descripción </label>
                    <h3>{this.state.dublin_descripcion}</h3>
                    {/* <input 
                            type="text" 
                            className="form-control"
                            value={this.state.dublin_descripcion}
                            onChange={this.onChangedublin_descripcion}
                            /> */}
                </div>
                <div className="form-group">
                    <label>Fuente </label>
                    <h3>{this.state.dublin_fuente}</h3>
                    {/* <input 
                            type="text" 
                            className="form-control"
                            value={this.state.dublin_fuente}
                            onChange={this.onChangedublin_fuente}
                            /> */}
                </div>

                <div className="form-group">
                    <label>Tipo </label>
                    <h3>{this.state.dublin_tipo_recurso}</h3>
                    {/* <div className="form-check form-check-inline">
                        <input  className="form-check-input" 
                                type="radio" 
                                name="tipoRecursoOptions" 
                                id="testimonio" 
                                value="Testimonio"
                                checked={this.state.dublin_tipo_recurso==='Testimonio'} 
                                onChange={this.onChangedublin_tipo_recurso}
                                />
                        <label className="form-check-label">Testimonio</label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input  className="form-check-input" 
                                type="radio" 
                                name="tipoRecursoOptions" 
                                id="informe" 
                                value="Informe" 
                                checked={this.state.dublin_tipo_recurso==='Informe'} 
                                onChange={this.onChangedublin_tipo_recurso}
                                />
                        <label className="form-check-label">Informe</label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input  className="form-check-input" 
                                type="radio" 
                                name="tipoRecursoOptions" 
                                id="caso" 
                                value="Caso" 
                                checked={this.state.dublin_tipo_recurso==='Caso'} 
                                onChange={this.onChangedublin_tipo_recurso}
                                />
                        <label className="form-check-label">Caso</label>
                    </div> */}
                </div>

                <div className="form-group">
                    <label>Cobertura </label>
                    <label>Feha Inicial </label>
                    <h3>{this.state.dublin_cobertura_fechaini}</h3>
                    {/* <input 
                            type="date" 
                            className="form-control"
                            dateFormat="yyyy-MM-dd"
                            value={this.state.dublin_cobertura_fechaini}
                            onChange={this.onChangedublin_cobertura_fechaini}
                            /> */}
                    <label>Feha Final </label>
                    <h3>{this.state.dublin_cobertura_fechafin}</h3>
                    {/* <input 
                            type="date" 
                            className="form-control"
                            dateFormat="yyyy-MM-dd"
                            value={this.state.dublin_cobertura_fechafin}
                            onChange={this.onChangedublin_cobertura_fechafin}
                            /> */}
                    
                </div>
                <div className="form-group">
                    <label>Ubicación </label>
                    <h3>{this.state.dublin_cobertura_ubicacion}</h3>
                    {/* <input 
                            type="text" 
                            className="form-control"
                            value={this.state.dublin_cobertura_ubicacion}
                            onChange={this.onChangedublin_cobertura_ubicacion}
                            /> */}
                </div>


                <div className="form-group">
                    <input type="submit" value="Borrar" className="btn btn-primary" />
                </div>
            </form>
            </div>
        )
    }
}

