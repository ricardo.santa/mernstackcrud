import React, { Component } from 'react';
import axios from 'axios';

export default class CreateTodo extends Component {

    constructor(props) {
        super(props);
    
        this.onChangedublin_titulo = this.onChangedublin_titulo.bind(this);
        this.onChangedublin_claves = this.onChangedublin_claves.bind(this);
        this.onChangedublin_descripcion = this.onChangedublin_descripcion.bind(this);
        this.onChangedublin_fuente = this.onChangedublin_fuente.bind(this);
        this.onChangedublin_tipo_recurso = this.onChangedublin_tipo_recurso.bind(this);
        this.onChangedublin_cobertura_fechaini = this.onChangedublin_cobertura_fechaini.bind(this);
        this.onChangedublin_cobertura_fechafin = this.onChangedublin_cobertura_fechafin.bind(this);
        this.onChangedublin_cobertura_ubicacion = this.onChangedublin_cobertura_ubicacion.bind(this);

        this.onSubmit = this.onSubmit.bind(this);
    
        this.state = {
            dublin_titulo: '',
            dublin_claves: '',
            dublin_descripcion: '',
            dublin_fuente: '',
            dublin_tipo_recurso: '',
            dublin_cobertura: {
                cobertura_fechaini: '',
                cobertura_fechafin: '',
                cobertura_ubicacion: ''
            }
        }
    }
    
    onChangedublin_titulo(e) {
        this.setState({
            dublin_titulo: e.target.value
        });
    }
    
    onChangedublin_claves(e) {              
        this.setState({
            dublin_claves: e.target.value
        });
    }
    onChangedublin_descripcion(e) {
        this.setState({
            dublin_descripcion: e.target.value
        });
    }
    onChangedublin_fuente(e) {
        this.setState({
            dublin_fuente: e.target.value
        });
    }
    onChangedublin_tipo_recurso(e) {
        this.setState({
            dublin_tipo_recurso: e.target.value
        });
    }
    onChangedublin_cobertura_fechaini(e) {
        this.setState({
            dublin_cobertura_fechaini: e.target.value
        });
    }

    onChangedublin_cobertura_fechafin(e) {
        this.setState({
            dublin_cobertura_fechafin: e.target.value
        });
    }

    onChangedublin_cobertura_ubicacion(e) {
        this.setState({
            dublin_cobertura_ubicacion: e.target.value
        });
    }
    
    
    onSubmit(e) {
        e.preventDefault();
        
        // console.log(`Form submitted:`);
        // console.log(`Todo Description: ${this.state.todo_description}`);
        // console.log(`Todo Responsible: ${this.state.todo_responsible}`);
        // console.log(`Todo Priority: ${this.state.todo_priority}`);
     
        // const newTodo = {
        //     todo_description: this.state.todo_description,
        //     todo_responsible: this.state.todo_responsible,
        //     todo_priority: this.state.todo_priority,
        //     todo_completed: this.state.todo_completed
        // };

        // .push a cada elemento ingresado.  se agrega por punto y coma
        var claves = this.state.dublin_claves.split(";");
        const newDublin = {
            dublin_titulo: this.state.dublin_titulo,
            dublin_claves: claves,
            dublin_descripcion: this.state.dublin_descripcion,
            dublin_fuente: this.state.dublin_fuente,
            dublin_tipo_recurso: this.state.dublin_tipo_recurso,
            dublin_cobertura: {
                    cobertura_fechaini: this.state.dublin_cobertura_fechaini,
                    cobertura_fechafin: this.state.dublin_cobertura_fechafin,
                    cobertura_ubicacion: this.state.dublin_cobertura_ubicacion
                    }
            }; 
        console.log(`Fecha ini: ${newDublin}`);


        //axios.post('http://localhost:4000/todos/add', newDublin)
        axios.post('https://crud01rsanta.herokuapp.com/todos/add', newDublin)
        
            .then(res => console.log(res.data));
        
        this.props.history.push('/');
        this.setState({
            dublin_titulo: '',
            dublin_claves: '',
            dublin_descripcion: '',
            dublin_fuente: '',
            dublin_tipo_recurso: '',
            dublin_cobertura_fechaini: '',
            dublin_cobertura_fechafin: '',
            dublin_cobertura_ubicacion: ''
        })
    }
    
    
    render() {
        return (
            <div style={{marginTop: 10}}>
            <h3>Registrar nuevo documento Dublin Core</h3>
            <form onSubmit={this.onSubmit}>
                <div className="form-group"> 
                    <label>Titulo: </label>
                    <input  type="text"
                            className="form-control"
                            value={this.state.dublin_titulo}
                            onChange={this.onChangedublin_titulo}
                            />
                </div>
                <div className="form-group">
                    <label>Claves: [separada por ;] </label>
                    <input 
                            type="text" 
                            className="form-control"
                            value={this.state.dublin_claves}
                            onChange={this.onChangedublin_claves}
                            />
                </div>
                <div className="form-group">
                    <label>Descripción </label>
                    <input 
                            type="text" 
                            className="form-control"
                            value={this.state.dublin_descripcion}
                            onChange={this.onChangedublin_descripcion}
                            />
                </div>
                <div className="form-group">
                    <label>Fuente </label>
                    <input 
                            type="text" 
                            className="form-control"
                            value={this.state.dublin_fuente}
                            onChange={this.onChangedublin_fuente}
                            />
                </div>

                <div className="form-group">
                    <div className="form-check form-check-inline">
                        <input  className="form-check-input" 
                                type="radio" 
                                name="tipoRecursoOptions" 
                                id="testimonio" 
                                value="Testimonio"
                                checked={this.state.dublin_tipo_recurso==='Testimonio'} 
                                onChange={this.onChangedublin_tipo_recurso}
                                />
                        <label className="form-check-label">Testimonio</label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input  className="form-check-input" 
                                type="radio" 
                                name="tipoRecursoOptions" 
                                id="informe" 
                                value="Informe" 
                                checked={this.state.dublin_tipo_recurso==='Informe'} 
                                onChange={this.onChangedublin_tipo_recurso}
                                />
                        <label className="form-check-label">Informe</label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input  className="form-check-input" 
                                type="radio" 
                                name="tipoRecursoOptions" 
                                id="caso" 
                                value="Caso" 
                                checked={this.state.dublin_tipo_recurso==='Caso'} 
                                onChange={this.onChangedublin_tipo_recurso}
                                />
                        <label className="form-check-label">Caso</label>
                    </div>
                </div>

                <div className="form-group">
                    <label>Cobertura </label>
                    <label>Feha Inicial </label>
                    <input 
                            type="date" 
                            className="form-control"
                            dateFormat="yyyy-MM-dd"
                            value={this.state.dublin_cobertura_fechaini}
                            onChange={this.onChangedublin_cobertura_fechaini}
                            />
                    <label>Feha Final </label>
                    <input 
                            type="date" 
                            className="form-control"
                            dateFormat="yyyy-MM-dd"
                            value={this.state.dublin_cobertura_fechafin}
                            onChange={this.onChangedublin_cobertura_fechafin}
                            />
                    
                </div>
                <div className="form-group">
                    <label>Ubicación </label>
                    <input 
                            type="text" 
                            className="form-control"
                            value={this.state.dublin_cobertura_ubicacion}
                            onChange={this.onChangedublin_cobertura_ubicacion}
                            />
                </div>


                <div className="form-group">
                    <input type="submit" value="Create Todo" className="btn btn-primary" />
                </div>
            </form>
        </div>
        )
    }
}

