import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Documentos = props => (
    <tr>
        <td>{props.dublin.dublin_titulo}</td>
        <td>
        <div>
            {props.dublin.dublin_claves
            .map(t => <p>{t}</p>)
            .reduce((prev, curr) => [prev, ' ', curr])}
        </div>
        </td>

        <td>{props.dublin.dublin_descripcion}</td>
        <td>{props.dublin.dublin_fuente}</td>
        <td>{props.dublin.dublin_tipo_recurso}</td>
        {/* <td>{props.dublin.dublin_cobertura.cobertura_fechaini}</td>
        <td>{props.dublin.dublin_cobertura.cobertura_fechafin}</td>
        <td>{props.dublin.dublin_cobertura.cobertura_ubicacion}</td> */}
        
        <td>
            <Link to={"/read/"+props.dublin._id}>Consultar</Link>
            <hr></hr>
            <Link to={"/edit/"+props.dublin._id}>Modificar</Link>
            <hr></hr>
            <Link to={"/delete/"+props.dublin._id}>Borrar</Link>
        </td>
    </tr>
)

export default class TodosList extends Component {

    constructor(props) {
        super(props);
        this.state = {dublin: []};
    }

    componentDidMount() {
        //axios.get('http://localhost:4000/todos/')
        axios.get('https://crud01rsanta.herokuapp.com/todos/')
        
            .then(response => {
                this.setState({ dublin: response.data });
            })
            .catch(function (error){
                console.log(error);
            })
    }

    DocList() {
        return this.state.dublin.map(function(currentDublin, i){
            // var claves = currentDublin.claves.join();
            return <Documentos dublin={currentDublin} key={i} />;
        })
    }

    render() {
        return (
            <div>
                <h3>Lista de documentos</h3>
                <table className="table table-striped" style={{ marginTop: 20 }} >
                    <thead>
                        <tr>
                            <th>Titulo</th>
                            <th>Claves</th>
                            <th>Descripción</th>
                            <th>Fuente</th>
                            <th>Tipo</th>
                        </tr>
                    </thead>
                    <tbody>
                        { this.DocList() }
                    </tbody>
                </table>
            </div>
        )
    }
}