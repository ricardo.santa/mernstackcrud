import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import CreateTodo from "../src/components/create-todo.component";
import ReadTodo from "../src/components/read-todo.component";
import EditTodo from "../src/components/edit-todo.component";
import DeleteTodo from "../src/components/delete-todo.component";
import TodosList from "../src/components/todos-list.component";


class App extends Component {
  render() {
    return (
      <Router>
      <div className="container">
          <Link to="/" className="navbar-brand">MERN-Stack App</Link>
          <hr></hr>
          <label>Comisión de la Verdad</label>
          <hr></hr>
          <label>CRUD Documento Dublin Core</label>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="https://gitlab.com/ricardo.santa/mernstackcrud.git" target="_blank">
            <img src={logo} width="80" height="80" alt="CodingTheSmartWay.com" />
          </a>
          <div className="collpase navbar-collapse">
            <ul className="navbar-nav mr-auto">
              <li className="navbar-item">
                <Link to="/" className="nav-link">Lista de documentos</Link>
              </li>
              <li className="navbar-item">
                <Link to="/create" className="nav-link">Crear documento</Link>
              </li>
            </ul>
          </div>
        </nav>
        <br/>
        <Route path="/" exact component={TodosList} />
        <Route path="/edit/:id" component={EditTodo} />
        <Route path="/delete/:id" component={DeleteTodo} />
        <Route path="/read/:id" component={ReadTodo} />
        <Route path="/create" component={CreateTodo} />
      </div>
    </Router>
    );
  }
}

export default App;
