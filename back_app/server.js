const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const todoRoutes = express.Router();
const PORT = 4000;

//let Todo = require('./todo.model');
let Dublin = require('./dublin.model');

// Set up a whitelist and check against it:
var whitelist = ['http://comisionverdad.softwareultimate.com'];
var corsOptions = {
  origin: function (origin, callback) {
    callback(null, true)
    // if (whitelist.indexOf(origin) !== -1) {
    //   callback(null, true)
    // } else {
    //   callback(new Error('Not allowed by CORS'))
    // }
  }
}

// Then pass them to cors:
app.use(cors(corsOptions));
//app.use(cors());

app.use(bodyParser.json());

//mongoose.connect('mongodb://127.0.0.1:27017/todos', { useNewUrlParser: true });
//mongoose.connect('mongodb://127.0.0.1:27017/dublin', { useNewUrlParser: true });
mongoose.connect('mongodb://uaikxg8vhrhothptddet:lz6RypU2NANr0D2ysxL1@bucyv02unkupori-mongodb.services.clever-cloud.com:27017/bucyv02unkupori', { useNewUrlParser: true });
const connection = mongoose.connection;

connection.once('open', function() {
    console.log("MongoDB database connection established successfully");
})

todoRoutes.route('/').get(function(req, res) {
    Dublin.find(function(err, dublins) {
        if (err) {
            console.log(err);
        } else {
            res.json(dublins);
        }
    });
});

todoRoutes.route('/:id').get(function(req, res) {
    let id = req.params.id;
    Dublin.findById(id, function(err, dublin) {
        res.json(dublin);
    });
});

todoRoutes.route('/delete/:id').post(function(req, res) {

    Dublin.findById(req.params.id, function(err, dublin) {
        if (!dublin)
            res.status(404).send("data is not found");
        else
            dublin.deleteOne().then(dublin => {
                res.json('dublin delete!');
            })
            .catch(err => {
                res.status(400).send("delete not possible");
            });
    });
});

todoRoutes.route('/update/:id').post(function(req, res) {
    Dublin.findById(req.params.id, function(err, dublin) {
        if (!dublin)
            res.status(404).send("data is not found");
        else
            dublin.dublin_titulo = req.body.dublin_titulo;
            dublin.dublin_claves = req.body.dublin_claves;
            dublin.dublin_descripcion = req.body.dublin_descripcion;
            dublin.dublin_fuente = req.body.dublin_fuente;
            dublin.dublin_tipo_recurso = req.body.dublin_tipo_recurso;
            //---- cobertura: fechas y ubicación
            dublin.dublin_cobertura.cobertura_fechaini = req.body.dublin_cobertura.cobertura_fechaini;
            dublin.dublin_cobertura.cobertura_fechafin = req.body.dublin_cobertura.cobertura_fechafin;
            dublin.dublin_cobertura.cobertura_ubicacion = req.body.dublin_cobertura.cobertura_ubicacion;

            dublin.save().then(dublin => {
                res.json('dublin updated!');
            })
            .catch(err => {
                res.status(400).send("Update not possible");
            });
    });
});

todoRoutes.route('/add').post(function(req, res) {
    let dublin = new Dublin(req.body);
    dublin.save()
        .then(dublin => {
            res.status(200).json({'dublin': 'dublin added successfully'});
        })
        .catch(err => {
            res.status(400).send('adding new dublin failed');
        });
});

app.use('/todos', todoRoutes);

app.listen(process.env.PORT || PORT, function() {
    console.log("Server is running on Port: " + process.env.PORT || PORT);
});