const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let dublin = new Schema({
    dublin_titulo: {
        type: String
    },
    dublin_claves: {        // Lista de cadenas de texto con los temas del recurso
        type: [String]
    },
    dublin_descripcion: {
        type: String
    },
    dublin_fuente: { 
        type: String
    },
    dublin_tipo_recurso: {
        type: String,
        enum: ["Testimonio", "Informe", "Caso"]
    },
    dublin_cobertura: {     // 
        cobertura_fechaini: {
            type: Date
        },
        cobertura_fechafin: {
            type: Date
        },
        cobertura_ubicacion: {
            type: String
        }
    }
});


module.exports = mongoose.model('Dublin', dublin);